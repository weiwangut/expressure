import dash

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
external_scripts = ['https://cdn.plot.ly/plotly-2.4.2.min.js']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets, external_scripts=external_scripts) ## just to define the only-one-app for others.
app.title = "Expressure"
server = app.server 

