import dash
import os
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
from dash.exceptions import PreventUpdate
import plotly.express as px
import plotly.graph_objects as go
import numpy as np
import pandas as pd
from datetime import date
from style import *
import dbAccess
from app import app
import base64
import json


data_file = "../demo/data/0313_wei_s0_10kg.txt"
DATAFILE_FOLDER = "/home/ec2-user/data/expr-dev-0/"

############ Utility functions #############

## Tell sequence from timestamp(milliseconds)
def isTcol(values):  
    if max(values) > 1024: 
        return True
    else:
        return False


def loadData(data_file):
    d = pd.read_csv(data_file, header = None, prefix="ch")
    
    if isTcol(d["ch0"]):
        d.rename(columns={'ch0':'time'}, inplace=True)
        d.drop(columns=["ch1"], inplace=True)
    else:
        d.drop(columns=["ch0"], inplace=True)
    cols = np.where(np.logical_and(d<6000, d>100).any()>0)[0] ## show only non-empty channels
    
    if 'time' in list(d.columns) and 0 not in cols:
        cols = np.append([0], cols)
    # print("The cols we need are: ", cols)
    if len(cols) == 0:
        cols = [1]
    return d.iloc[:,cols]    


def checkFileName(fname=None):
    if fname is None:
        return [False, ""]
    if len(fname.split('_'))!=4 or fname[-4:] != ".txt":
        return [False, "File name not correct!!! Should be: 'time_submitter_sensortype_info.txt'"]
    else:
        return [True, "File name check success."]
        
############ Callbacks Home Page #######
@app.callback(Output('output-data-upload', 'children'),
              Input('upload-data', 'contents'),
              State('upload-data', 'filename'),
              State('upload-data', 'last_modified'))
def update_file_upload(content, name, date):
    rc, message = checkFileName(name)
    if rc == False:
        return message
    with open(DATAFILE_FOLDER + name, 'w') as file:
        b64value = content.split('base64,')[1]
        text = base64.b64decode(b64value).decode()
        file.write(text)
    db = dbAccess.dbDev('expr-dev', 'phase')
    db.insertFile(DATAFILE_FOLDER + name)
    return 'Upload success: '+ name

@app.callback(Output('xhr-channel', 'children'),
              Input('xhr-channel', 'data-bleSave'))
def update_xhr_datasave(p):
    if p is None:
        raise PreventUpdate

    fpath, data = DATAFILE_FOLDER+p['f'], p['d']
    try:
        with open(fpath, 'w') as file:
            file.write(data)
        db = dbAccess.dbDev('expr-dev', 'phase')
        db.insertFile(fpath)
    except:
        return PreventUpdate
    return PreventUpdate
    # else:
    #     data = p['d'].split('\n')[:-1]
    #     data = np.array([ np.array(d.split(',')) for d in data ]).astype(int)
    #     dt = data.T
        
    #     t = dt[0]/1000.0
    #     cols = np.where([ (d>100).any() for d in dt])[0]

    #     for c in cols[1:]:
    #         print('valid data: ', len(cols))
    #         fig.add_trace(go.Scatter(x=t, y=dt[c], name=str(c), mode='lines+markers',))
        
    #     fig.update_layout(
    #         xaxis_title="Time (s)",
    #         yaxis_title="Measurement (ohm)"
    #     )
    #     print(fig)
    #     return fig



############ Callbacks Show Page ###################
@app.callback(
    Output('select-data', 'options'),
    [Input('select-submit', 'value'), Input('select-sensor', 'value'),  Input('select-info', 'value'),] 
)
def update_select_data(submit, sensor, info):
    db = dbAccess.dbDev('expr-dev', 'phase')
    scanArgs = dict()
    for k,v in zip( ["submitter", "sensor", "info"], [submit, sensor, info] ):
        if v !=None and len(v) > 0:
            scanArgs[k] = v
    items = db.scanItems('prototype', scanArgs)
    
    # return [ {"label":i, "value":i} for i in [submit, date, sensor] if i is not None ] + [ {"label":"data_file", "value":data_file} ]
    return [ {"label":p['datafile'].split('/')[-1], "value":p['datafile']} for p in items ]


@app.callback(
    [Output('plot-data', 'figure'), Output('store-df', 'data')],
    [Input('select-data', 'value')]
)
def update_plot_data(data_file):
    fig = go.Figure()   
    fig.update_layout(margin=dict(l=20, r=20, t=20, b=20))
    if data_file is None or not os.access(data_file, os.F_OK):
        print("file not exist")
        return fig,{}

    df = loadData(data_file)
    if 'time' in df.columns:
        t = df['time']/1000.0 ## to seconds
        df.drop(columns = 'time', inplace=True)
    else:
        t = np.arange(0, df.shape[0]/30-.01, 1/30) ## need refine
        df.index = t

    for c in df.columns:
        y = df[c]
        y[y==0] = None ## plot gaps in line
        fig.add_trace(go.Scatter(x=t, y=df[c], name=c, mode='lines+markers',))
    fig.update_layout(
        xaxis_title="Time (s)",
        yaxis_title="Measurement (ohm)"
    )

    return fig, df.to_json(date_format='iso', orient='split')

@app.callback(
    Output('plot-stats', 'figure'),
    [Input('plot-data', 'relayoutData'), State('store-df', 'data')]
)
def update_plot_stats(relayoutData, df_data={}): ## change the stats with axis.
    fig = go.Figure()   
    if df_data == {} or df_data is None:
        return fig
    df = pd.read_json(df_data, orient='split')
    channels = df.columns
    if "xaxis.range[0]" in relayoutData.keys():
        df = df.loc[ (df.index>relayoutData['xaxis.range[0]'])
                     & (df.index<relayoutData['xaxis.range[1]']), :]
        df[ (df<relayoutData['yaxis.range[0]']) 
                    | (df>relayoutData['yaxis.range[1]']) ] = 0

    print((df>0).mean())
    fig.add_trace(go.Bar(y=channels, x=df[df>0].mean(), orientation='h', name = 'mean'))
    fig.add_trace(go.Bar(y=channels, x=df[df>0].median(), orientation='h', name = 'median'))
    fig.update_layout(margin=dict(l=20, r=20, t=20, b=20))

    return fig

## The actual init function 
@app.callback(
    [Output('select-submit', 'options'), Output('select-sensor', 'options'),  Output('select-info', 'options')],
    [Input('onload', 'children')]
)
def update_select_datafile(d): 
    db = dbAccess.dbDev('expr-dev', 'phase')
    opsub = [ {'label':v, 'value':v } for v in db.getDinstinctAttr( 'prototype', 'submitter') ]
    opdate = [ {'label':v, 'value':v } for v in db.getDinstinctAttr( 'prototype', 'sensor') ]
    opsensor = [ {'label':v, 'value':v } for v in db.getDinstinctAttr( 'prototype', 'info') ]
    return opsub,opdate,opsensor


## Delete file and db
@app.callback(
    Output('output-delete', 'children'),
    Input('delete-file', 'n_clicks'),
    State('select-data', 'value')
)
def update_file_delete(n, fpath): 
    if fpath is None:
        return "Delete this file !!"
    name = os.path.basename(fpath)
    
    if name.split('_')[1] == 'wei':
        return "Can't delete admin file"
    os.remove(fpath)
    db = dbAccess.dbDev('expr-dev', 'phase')
    db.deleteFile(fpath)
    return "file deleted: "+name


## Download selected file
@app.callback(
    Output('download-file', 'data'),
    [ Input('download-btn', 'n_clicks'),
      State('select-data', 'value')],
    prevent_initial_call=True
)
def update_file_download(n, fpath): 
    print("download file: ", fpath)
    if fpath is None:
        return {}
    return dcc.send_file(fpath)

