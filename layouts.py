import dash
import dash_core_components as dcc
import dash_html_components as html
# import grasia_dash_components as gdc
import plotly.express as px
import plotly.graph_objects as go
from dash.dependencies import Input, Output, State
import numpy as np
import pandas as pd
from datetime import date
import os

############ Data Display Layout ###################
layoutDev = html.Div(children=[
    html.Div( # Row - 0 : title
        className="row",
        children = [html.H1("Demo-test : Sensor-data visualization ", className='header0')]
    ),

    html.Div( # Row - 1 : Data selection
        className="row",
        children = [
            html.Div(
                className = "two columns div-user-controls",
                children = [
                    html.P("Submitter"),
                    html.Div(
                        className="div-for-dropdown",
                        children=[
                            dcc.Dropdown(
                                id="select-submit",
                                options=[],
                                multi=False,
                                placeholder="filter by submitter",
                                style={"border": "2px solid black"},
                            )
                        ],
                    ),]
            ),

            html.Div(
                className = "two columns div-user-controls",
                children = [
                    html.P("Sensor type"),
                    html.Div(
                        className="div-for-dropdown",
                        children=[
                            dcc.Dropdown(
                                id="select-sensor",
                                options=[],
                                multi=False,
                                placeholder="filter by sensor",
                                style={"border": "2px solid black"},
                            )
                        ],
                    ),]
            ),           

            html.Div(
                className = "two columns div-user-controls",
                children = [
                    html.P("Custom Info"),
                    html.Div(
                        className="div-for-dropdown",
                        children=[
                            dcc.Dropdown(
                                id="select-info",
                                options=[],
                                multi=False,
                                placeholder="filter by info",
                                style={"border": "2px solid black"},
                            )
                        ],
                    ),]
            ), 

            html.Div(
                className = "four columns div-user-controls",
                children = [
                    html.P("Select the file to plot"),
                    html.Div(
                        className="div-for-dropdown",
                        children=[
                            dcc.Dropdown(
                                id="select-data",
                                options=[],
                                multi=False,
                                placeholder="Select the data file",
                                style={"border": "2px solid black"},
                            )
                        ],
                    ),]
            ), 
            
            html.Div(
                className = "two columns div-user-controls",
                children = [
                    html.P("Delete/Download selected file", id="output-delete"),
                    html.Button("Delete !!", id="delete-file"), 
                    html.P(""),
                    html.Button("Download", id="download-btn"), 
                    dcc.Download(id="download-file"),
                ]
            ), 

    ]),

    html.Div( # Row - 2 : Graphs: Raw-data,  Statistics, Filtered(optional)
        className="row",
        children = [
            html.Div(
                className = "ten columns my-container",
                children = [
                    html.P("Figure : sensor points"),
                    html.Div(
                        dcc.Graph(id="plot-data"),
                    ),]
            ),

            html.Div(
                className = "two columns my-container",
                children = [
                    html.P("plot-stats"),
                    html.Div(
                        dcc.Graph(id="plot-stats"),
                    ),]
            ),           

    ]),

    html.Div(id = 'onload'),
    
    
    dcc.Link('Back to home page', href='/'),

    dcc.Store(id = 'store-df', storage_type='session'),

    dcc.Interval( 
    id = 'button-timer', 
    interval = 1000, 
    disabled = True,
    n_intervals = 0),

])


############ Home Page Layout ###################
layoutHome = html.Div(children=[
    html.Div( # Row - 0 : title
        className="row",
        children = [html.H1("Demo : Data management", className='header0')]  
    ),
    
    html.Div( # Row - 1 : BLE
        className="row",
        children = [
            html.Div( 
                className="two columns",
                children = html.Button("BLE start", className='button', id='ble-scan'), ## front-end button
            ),
            
            html.Div( 
                className="four columns",
                children = html.Div("", id='output-ble-scan'),
            ),

        ]),
        
    html.Div( # Row - 2 : File settings
        className = "row",
        children = [
            html.Div( 
                className="two columns",
                children = html.Button("Save data:", className='button', id='ble-save'), ## front-end button
            ),
            
            html.Div( 
                className="five columns",
                children = dcc.Input(type='text', size='70', value="time_submitter_sensor_info.txt",  id='ble-fname')
            ),
            html.Div( 
                className="four columns",
                children = html.Div(id='output-ble-save'),
            ),   

        ]
    ),
    
    html.Div( # Row - 3 : upload local files
        className="row",
        children = [
            dcc.Upload(
                id='upload-data',
                children=html.Div([ 'Drag and Drop or ', html.A('Select Files')]),
                style={
                    'width': '100%',
                    'height': '60px',
                    'lineHeight': '60px',
                    'borderWidth': '1px',
                    'borderStyle': 'dashed',
                    'borderRadius': '5px',
                    'textAlign': 'center',
                    'margin': '10px'
                },
                # Allow multiple files to be uploaded
                multiple=False
            ),
            html.Div(id='output-data-upload'),
        ]
    ),
    

    dcc.Link('Navigate to Show Sensor Data', href='/show'),

    html.Div( # Row - 4 : realtime data
        id = "plot-realtime",
        children = 'plot real time'
    ),

    # html.Div( # Row - 4 : Graph: Raw-data
    #     className="row",
    #     children = [
    #         html.Div(
    #             children = [
    #                 html.P("Figure : realtime sensor points"),
    #                 html.Div(
    #                     dcc.Graph(id="plot-data-realtime"),
    #                 ),]
    #         ),
    # ]),

    ##### Core components #######
    html.Div(id='ble-data', hidden=True),
    html.Button('hidden', id='xhr-channel', hidden=True), ## persudo-button for XHR callback
    html.Div('test-div', id='test-div'),
])

