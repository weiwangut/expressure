import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

from app import app
from layouts import layoutDev, layoutHome
import callbacks

## The full layouts, just to validate callbacks
app.validation_layout = html.Div([
    layoutDev,
    layoutHome,
    html.Div([
        dcc.Location(id='url', refresh=False),
        html.Div(id='page-content')
    ])
])

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content') ## For multi-pages: the entire page is returned by the callback
])


@app.callback(Output('page-content', 'children'),
              Input('url', 'pathname'), 
              State('url', 'search'))
def display_page(pathname, search):
    ''' Dispatch to multi-pages according to url '''
    print(pathname, search)  ## pathname = xx.com, search = /xx?a=xx
    if pathname == '/':
        return layoutHome 
    elif pathname == '/show':
        return layoutDev
    else:
        return '404'

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', debug=True, port=8080,
        ssl_context=('/home/ec2-user/data/CA/my.aws.crt', '/home/ec2-user/data/CA/my.aws.key') )
    # app.run_server(host='0.0.0.0', debug=True)