#include <ArduinoBLE.h>
#include <PDM.h>
#include "functions.h"

const char* nameOfPeripheral = "PressureMeasure";
const char* uuidOfService = "00001101-0000-1000-8000-00805f9b34fb";
const char* uuidOfRxChar = "00001142-0000-1000-8000-00805f9b34fb";
const char* uuidOfTxChar = "00001143-0000-1000-8000-00805f9b34fb";

// Setup the incoming data characteristic (RX).
short sensorData[sensorPoints] = {0}; // 16-bits per reading
static char buff[BUFFER_TX] = {0}; // used to store BLE data
volatile int samplesRead;

// Setup BLE Service, rxchar, txchar
BLEService generalService(uuidOfService);
BLECharacteristic rxChar(uuidOfRxChar, BLEWriteWithoutResponse | BLEWrite, BUFFER_RX, false); // can read string
BLECharacteristic txChar(uuidOfTxChar, BLERead | BLENotify | BLEBroadcast, sizeof(buff), false); // can send byte

void setupServices()
{
// Create BLE service and characteristics.
  BLE.setLocalName(nameOfPeripheral);
  BLE.setAdvertisedService(generalService);
  generalService.addCharacteristic(rxChar);
  generalService.addCharacteristic(txChar);
  BLE.addService(generalService);

  // Bluetooth LE connection handlers.
  BLE.setEventHandler(BLEConnected, onBLEConnected);
  BLE.setEventHandler(BLEDisconnected, onBLEDisconnected);
  rxChar.setEventHandler(BLEWritten, onRxCharValueUpdate);

    // Let's tell devices about us.
  BLE.advertise();
  // Print out full UUID and MAC address.
  Serial.println("Peripheral advertising info: ");
  Serial.print("Name: ");
  Serial.println(nameOfPeripheral);
  Serial.print("MAC: ");
  Serial.println(BLE.address());
  Serial.print("Service UUID: ");
  Serial.println(generalService.uuid());
  Serial.print("rxCharacteristic UUID: ");
  Serial.println(uuidOfRxChar);
  Serial.print("txCharacteristics UUID: ");
  Serial.println(uuidOfTxChar);
  Serial.println("Bluetooth device active, waiting for connections...");
}

void setup()
{
  Serial.begin(9600);
  Serial.println("Init start!");
  
  pinMode(LEDR, OUTPUT);
  pinMode(LEDB, OUTPUT);
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDB, HIGH); // low-high reversed actually.
  
  startBLE();
  setupServices();
  sensorInit();
  
  Serial.println("Init done!");
  digitalWrite(LEDR, HIGH);
  
}


void loop() {
  BLEDevice central = BLE.central();
  sensorMeasure(sensorData);
//  sensorMeasureSingle(sensorData);
  
  delay(DELAY_LOOP);
  if (central)
  {
    // Only send data if we are connected to a central device.
    if (central.connected())
    {
      connectedLight();
      txBulk(sensorData, sensorPoints);
    }
    else{
      disconnectedLight();
      delay(1);
    }
  }
}


void startBLE() {
  if (!BLE.begin())
  {
    Serial.println("starting BLE failed!");
    while(1);
  }
}

void onRxCharValueUpdate(BLEDevice central, BLECharacteristic characteristic) {
  // central wrote new value to characteristic, update LED
  Serial.print("Characteristic event, read: ");
  byte test[256];
  int dataLength = rxChar.readValue(test, 256);

  for(int i = 0; i < dataLength; i++) {
    Serial.print((char)test[i]);
  }
  Serial.println();
  Serial.print("Value length = ");
  Serial.println(rxChar.valueLength());
}

void onBLEConnected(BLEDevice central) {
  Serial.print("Connected event, central: ");
  Serial.println(central.address());
  connectedLight();
}

void onBLEDisconnected(BLEDevice central) {
  Serial.print("Disconnected event, central: ");
  Serial.println(central.address());
  disconnectedLight();
}


// *s : string to calculate crc
// len: length of string
// code: an extra magic code
// return: crc value
char crc_(byte *s, int len, char code)
{
  byte c = 0;
  byte *tmp = s;
  while(s - tmp < len)
      c ^= *s++ + code;
  return c;
}


// *packet & len : input data & length.
//  return: output length.
//  !! Now is a 'fake' encode function, just replace 0 !!
int encode(char* packet, int len) 
{
  for(int i=0; i<len; i++)
  {
    if(packet[i] == 255)
      packet[i] = 254;
    else if(packet[i] == 0)
      packet[i] = 255;
  }
  return len; // output length.
}

void txBulk(short* data, int len)
{
  static byte seq = 2;
  int pos = 0;
  int payload_len = 10; // packet length should be byte.

  buff[pos++] = MAGIC_HEAD>>8;
  buff[pos++] = MAGIC_HEAD&0x00ff;
  buff[pos++] = seq;
  memcpy(buff+pos, data, 2*sensorPoints);
  payload_len = encode(buff+pos, 2*sensorPoints);
  pos += payload_len;
  buff[pos] = crc_( (byte*)buff, pos, MAGIC_CRC);
  buff[++pos] = 0; // ending bit
  txChar.writeValue(buff, pos, false); // length = 'pos', ending-bit doesn't count

  if (seq ++ == 254) // !!! Skip 0,1 because unknown bug truncated the data !!!!
    seq = 2; 
}


/*
 * LEDS
 */
void connectedLight() {
  digitalWrite(LEDB, LOW);
}


void disconnectedLight() {
  digitalWrite(LEDB, HIGH);
}
