#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <ArduinoBLE.h>

#define numRows 7
#define numCols 10
#define sensorPoints 70
#define MAGIC_HEAD 0x1234
#define MAGIC_CRC 10
#define BUFFER_RX 256
#define BUFFER_TX 256
#define DELAY_LOOP 0 // a delay between loops 


void connectedLight();
void disconnectedLight();
void startBLE();

void onBLEConnected(BLEDevice central);
void onBLEDisconnected(BLEDevice central);
void onRxCharValueUpdate(BLEDevice central, BLECharacteristic characteristic);
char crc_(byte *s, int len, char code);
void txBulk(short* data, int len);

void sensorInit();
void sensorMeasure(short* data);
void sensorMeasureSingle(short* data);


#endif 
