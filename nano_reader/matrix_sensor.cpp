#include <ArduinoBLE.h>
#include <PDM.h>
#include "functions.h"


int rows[] = {A1, A2, A3, A4, A5, A6, A7};
int cols[] = {2,3,4,5,6,7,8,9,10,11};


void sensorInit() {
// set all rows and columns to INPUT (high impedance):
  for (int i = 0; i < numRows; i++) {
    pinMode(rows[i], INPUT_PULLUP);
  }
  for (int i = 0; i < numCols; i++) {
    pinMode(cols[i], INPUT);
  }
}

void sensorMeasure(short* data) {
  for (int colCount = 0; colCount < numCols; colCount++) {
    pinMode(cols[colCount], OUTPUT); // set as OUTPUT
    digitalWrite(cols[colCount], LOW); // set LOW
    
    for (int rowCount = 0; rowCount < numRows; rowCount++) {
      data[colCount * numRows + rowCount] = analogRead(rows[rowCount]); // read INPUT
    }
    pinMode(cols[colCount], INPUT); // set back to INPUT!
  }
  
  // Print the incoming values of the grid:
  for (int i = 0; i < sensorPoints; i++) {
    Serial.print(data[i]);
    Serial.print(" ");
  }
  Serial.println();
}

// measure each sensor at a time
void sensorMeasureSingle(short* data) {
  for (int colCount = 0; colCount < numCols; colCount++) {
    pinMode(cols[colCount], OUTPUT); 
    digitalWrite(cols[colCount], LOW); // set OUTPUT->GROUND, so A* give current
    
    for (int rowCount = 0; rowCount < numRows; rowCount++) {
      pinMode(rows[rowCount], INPUT_PULLUP);
      delay(1);
      data[colCount * numRows + rowCount] = analogRead(rows[rowCount]); // read INPUT
      pinMode(rows[rowCount], INPUT);
    }
    pinMode(cols[colCount], INPUT); // set INPUT(open)
  }
  
  // Print the incoming values of the grid:
  for (int i = 0; i < sensorPoints; i++) {
    Serial.print(data[i]);
    Serial.print(" ");
  }
  Serial.println();
}
