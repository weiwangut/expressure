import numpy as np 
import os
import boto3
from boto3.dynamodb.conditions import Key

### The database object for accessing development phase data
###   - Mostly as a wrapper for AWS API.
###
class dbDev():
    def __init__(self, tableName, primekey):
        self.client = boto3.client('dynamodb')
        self.table =  boto3.resource('dynamodb').Table(tableName)
        self.primekey = primekey
        # response = self.client.describe_table(TableName='expr-dev')
        try:
            print("Table name is: ", self.table.name)
            print("Table items :", self.table.item_count)
            print("Table size (bytes): ", self.table.table_size_bytes)
        except:
            print("Error, database connection fail!")
        return


    ## item is json
    def insertItem(self, item):
        # self.client.put_item(TableName = self.tableName, Item=item)
        self.table.put_item(Item=item)
        return


    ## A primary-key without sort key still returns multi-itmes
    def getItemsByKey(self,  key):
        response = self.table.query(
            KeyConditionExpression=Key(self.primekey).eq(key) # & Key('datafile').begins_with('/home')  
        )
        print(response['Items'])
        return
    
    ## To find the distinct values(string or number) of an attribute of all items
    ## Needs to first query the entire key
    def getDinstinctAttr(self,  key, attrName):
        response = self.table.query(
            KeyConditionExpression=Key(self.primekey).eq(key)
        )
        print( set([r[attrName] for r in response['Items']]) )
        return  set([r[attrName] for r in response['Items']])
        
    ## scan needs first query, so it's time costing.
    ## For simplicity, now only scan with attribute.eq(value)
    ##  - key : prime key value 
    ##  - attributes : a dictionary of the attributes
    def scanItems(self, key, attributes):
        filters = Key(self.primekey).eq(key)
        for k,v in attributes.items():
            print(k, v, type(k), type(v))
            filters = filters & Key(k).eq(v)
        scanArgs = {
            'FilterExpression': filters,
            'ProjectionExpression': "datafile"
        }
        response = self.table.scan(**scanArgs)
        # print(response)
        return response['Items']


    ## Load disk files into db
    ##   - folder: the path of csv
    ##   - file name format should be: time_submitter_sensor_info.txt
    def loadLocal(self, folder=None):
        files = os.listdir(folder)
        for f in files:
            fpath = folder + '/' + f
            self.insertFile(fpath)

        return

    ## Insert disk-file to dynamodb ###
    ## fname: the file already on local-disk 
    def insertFile(self, fpath=None):
        n = os.path.basename(fpath).split('.')[0].split('_')
        d = np.genfromtxt(fpath, delimiter=',')
        item = { 
               'phase': 'prototype', #{'S':'prototype'},
               'time': n[0],#{'S':n[0]},
               'submitter': n[1],#{'S':n[1]},
               'sensor': n[2],#{'S':n[2]},
               'info': n[3],#{'S':n[3]},
               'sample num': d.shape[0],#{'N': str(d.shape[0]) }, 
               'sensor num': d.shape[1],#{'N': str(d.shape[1]) },
               'datafile':fpath}#{'S':fpath}}
        self.insertItem(item)
        return
    
    ### delte entry ###
    def deleteFile(self, fpath=None):
        self.table.delete_item(Key = {'phase':'prototype', 'datafile':fpath})
        
if __name__ == "__main__":
    db = dbDev('expr-dev', 'phase')
    db.loadLocal('/home/ec2-user/data/expr-dev-0')

