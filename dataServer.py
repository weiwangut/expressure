from http.server import BaseHTTPRequestHandler, HTTPServer
import ssl
import dbAccess

class myHandler(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        print("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        
        query = self.path.split('?')[1]
        fpath = query.split('=')[1]
        data = post_data.decode('utf-8')
        
        try:
            with open(fpath, 'w') as file:
                file.write(data)
            db = dbAccess.dbDev('expr-dev', 'phase')
            db.insertFile(fpath)
        except:
            return
        return

httpd = HTTPServer(('', 8088), myHandler)
httpd.socket = ssl.wrap_socket (httpd.socket, certfile='/home/ec2-user/data/CA/my.aws.crt', 
                                keyfile='/home/ec2-user/data/CA/my.aws.key', server_side=True)
httpd.serve_forever()