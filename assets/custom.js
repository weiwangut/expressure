// import * as plotly from 'https://cdn.plot.ly/plotly-2.4.2.min.js' 

var SERVICE_ID="00001101-0000-1000-8000-00805f9b34fb"
var CHAR_ID = "00001143-0000-1000-8000-00805f9b34fb"
var DATA_LEN = 70
var sampleNum = 0

var DATAFILE_FOLDER = "/home/ec2-user/data/expr-dev-0/"
var initT = Date.now()

// convert the block string-data to 2d-array
function blockToArray(block){
  const myArr = block.split("\n"); 
  myArr.pop();
  row = myArr[0].split(',').length;
  col = myArr.length;
  
  var newArr = new Array(row);
  for (var i = 0; i < row; i++) {
    newArr[i] = new Array(col);
  }
  
  for(i=0; i<myArr.length; i++){
  	myArr[i] = myArr[i].split(',');
    		for(j=0; j<myArr[i].length; j++)	
          	newArr[j][i] = parseInt(myArr[i][j])
  }

  return newArr;
}

function handleNotifications(event){
  let buffer = event.target.value.buffer;
  let b = new Uint8Array(buffer); // original data

  b = b.slice(3,-1); // truncate magic number &  crc
  for (var i=0; i<b.length; i++) // Decode all 255=>0
    b[i] = b[i]==255?0:b[i];
    
  let d = new Uint32Array(2 + DATA_LEN); // timestamp - seq - data
  d[0] = Date.now() - initT;
  // console.log(d[0]);
  d[1] = b[0];
  for (var i=0; i<DATA_LEN; i++){
    let m = b[1+2*i]*256+b[2*i];
    let ohm = m<500?14000*m/(1024-m):0;
    d[i+1] = ohm;
  }
  
  document.getElementById('ble-data').innerHTML += d.toString()+'\n';
  sampleNum+=1;
  if(sampleNum % 50 == 0){
    document.getElementById('output-ble-scan').innerHTML  = "(success) getting data num: "+sampleNum;
  }
  
  if (sampleNum % 50 == 0){
    let dataStr = document.getElementById('ble-data').innerHTML;
    let dataArr = blockToArray(dataStr);
    let plotData = [];
    
    var times = dataArr[0];
    for (i=0; i<times.length; i++)
      times[i] /= 1000;
      
    for (i=1; i<dataArr.length; i++){
      for (let j=0; j<dataArr[0].length; j++){
        if (dataArr[i][j] > 100){
          plotData.push(dataArr[i]);
          console.log('data valid: ', i);
          break;
        }
      }
    } 
    
    console.log(times.length, plotData[0].length);
    let traces = [];
    for (i=0; i<plotData.length; i++){
      traces.push(
        {
          // x: [...plotData[i].keys()],
          x: times, 
          y: plotData[i],
          type: 'scatter'         
        }
      )
    }

    var layout = {autosize: false, width: 1500, height: 500};

    console.log('traces length: ', traces.length);
    Plotly.newPlot('plot-realtime', traces, layout);
  }
}


function onBleSave(){
  let fname = document.getElementById('ble-fname').value;
  let data = document.getElementById('ble-data').innerHTML;

  if(data.length == 0){
    document.getElementById('output-ble-save').innerHTML="No data";
    return
  }
  if(fname.split('_').length<4){
    document.getElementById('output-ble-save').innerHTML="File name wrong!! ->'time_submitter_sensor_info.txt'";
    return
  }
  document.getElementById('output-ble-save').innerHTML="saved data num: "+data.split('\n').length;
  xhrDataToCallback({'f':fname, 'd':data}, 'bleSave');
  
  // document.getElementById('output-ble-save').innerHTML="saved data num: "+data.split('\n').length;
  // var xhttp = new XMLHttpRequest();
  // xhttp.open("POST", "https://ec2-18-190-155-26.us-east-2.compute.amazonaws.com:8088/?f="+DATAFILE_FOLDER+fname,false);
  // // xhttp.open("GET", "https://ec2-18-190-155-26.us-east-2.compute.amazonaws.com:8080/save",false);
  // xhttp.send(data);
  // console.log('save status: ', xhttp.status) // wait until sent
  // return;  
}

function onBleScan() {
  document.getElementById('ble-data').innerHTML="";
  initT = Date.now();
  sampleNum = 0;
  
  let options = {filters: [{services: [SERVICE_ID]}]};
  // console.log(navigator, navigator.bluetooth)
  navigator.bluetooth.requestDevice(options)
  .then(device => {
    return device.gatt.connect();

  }).then(server  => {
    document.getElementById('output-ble-scan').innerHTML  = "(step-1)connected: "+server.connected;
    return server.getPrimaryService(SERVICE_ID);
  }).then(service =>{
    document.getElementById('output-ble-scan').innerHTML  = "(step-2)find service: "+CHAR_ID;
    return service.getCharacteristic(CHAR_ID);
  }).then(characteristic  =>{
    return characteristic.startNotifications()
  }).then( characteristic => {
    document.getElementById('output-ble-scan').innerHTML  = "(success) start getting data";
    characteristic.addEventListener('characteristicvaluechanged',
          handleNotifications);
  })
  .catch(error => {
    console.log('Argh! ' + error);
  });

}

// ch is a string to dispatch different callbacks, i.e. 'blesave', 'plot' 
function xhrDataToCallback(data, ch){
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", "/_dash-update-component",false);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  xhttp.setRequestHeader('Accept', 'application/json'); 
  xhttp.setRequestHeader('X-CSRFToken', 'undefined');

  let packet = JSON.stringify(
    {"output":"xhr-channel.children",
     "outputs":{"id":"xhr-channel","property":"children"},
     "inputs":[{"id":"xhr-channel","property":"data-"+ch,"value":data}],
     "changedPropIds":["test-button.data-"+ch]}
  );
  xhttp.send(packet);
}


function bindJs(){
  // console.log('load bind js');
  if (document.getElementById('ble-scan')){
    document.getElementById('ble-scan').onclick = onBleScan;
    document.getElementById('ble-save').onclick = onBleSave;
    // alert(window.location.pathname);
    document.getElementById('xhr-channel').style.visibility = 'hidden';
  }
  else{
    setTimeout(bindJs, 2000);
  }
}

window.onload = bindJs;

