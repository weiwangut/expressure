import plotly.express as px

colors = {
    'background': '#111111',
    'text': '#7FDBFF',
    'notation': '#7FFFFF'
}


def figStyle(id, fig):
    if id == 'graph-tsne':
        fig.update_layout(
        plot_bgcolor=colors['background'],
        paper_bgcolor=colors['background'],
        font_color=colors['text'],
        font_size = 12, 
        legend_font_size = 23,
        legend_title_font_size = 25)

    elif id == 'graph-histogram':
        fig.update_layout(
        plot_bgcolor=colors['background'],
        paper_bgcolor=colors['background'],
        font_color=colors['text'],
        font_size = 12, 
        legend_font_size = 23,
        legend_title_font_size = 25)    

    elif id == 'graph-timeline':
        fig.update_layout(
            # plot_bgcolor=colors['background'],
            template="seaborn",
            # paper_bgcolor=colors['background'],
            # font_color='#7FDBAA', 
            # font_size = 20
            )

    return fig
